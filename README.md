# GitOps-Jenkins-library

TODO: tee esimerkit Jenkins confista
TODO: huomaa, että 1 repo voi tehdä vain 1:n jokaista artifactityyppiä!

When implement GitOps flow in Jenkins, Jenkinsfiles tends to become unreadable.

This library contains common steps & classes to use in those files. Jenkinsfile should tell `what` and `how` part is in this shared library.

https://bmuschko.com/blog/jenkins-shared-libraries/

## How do I get set up?

You need to set up:

1. Jenkins Global setting to use shared library
1. Jenkinsfile import in actual project

### Jenkins setting

1. Navigate to `Jenkins > Manage Jenkins > Configure System`
1. Find `Global Pipeline Libraries` section on page
1. Configure
   - Name: `GitOps-Jenkins-library``
   - Default version: `master` or what ever you like to use
   - Modern SCM: `checked`
   - Git:
     - Project Repository: `https://bitbucket.org/teammoilanen/gitops-jenkins-library`
     - Creditentials: Your access to repo (now its private)

Correct settings look like this:
![setting](wiki/images/jenkins.setting.png)

### Consume `GitOps-Jenkins-library` shared library in your `Jenkinsfile`

```groovy
@Library('GitOps-Jenkins-library')_
import com.dozenthing.jenkins.<CLASS> // when use classes of GitOps-Jenkins-library

// pipeline usual way
```

## Repositories in Project

### Microservice | UI

Project has typically multiple `Microservice` and/or `UI` repos.

### Integration test

Project contains one `Integration test` repo.

### E2E test

Project contains one `E2E test` repo.

### Manual acceptance

When decide to use manual acceptence step for `Microservice | UI` repo, ...

### Deploy

Project must have `1 and only 1` Git repo for deployment with name `deployment`.

Project must have Jenkins job with name `deploy`.

## Deployment Git repo

1. All microservices & UI projects are located on `deployment` project root in own folder.
1. Folder name must be `<REPO_SLUG>`
1. Microservice has own `values yaml file` with name `values-<ENVIRONMENT>.yaml` for each environment to deploy. File must be located on `<REPO_SLUG>` folder.
   > Where `ENVIRONMENT` is one of `integration | e2e | development | stage | production`
1. `values-<ENVIRONMENT>.yaml` file must have field `container.version` which is used in `Heml v3 chart` to used as container version.

Jenkinsfile for deployment project:

```groovy
@Library('GitOpsFlow-Jenkins-library')_
import com.dozenthing.jenkins.*

node {
  properties([
    parameters ([
      string(defaultValue: "", name: 'file', trim: true, description: "File to change. Must be YAML file."),
      string(defaultValue: "", name: 'key', trim: true, description: "like 'container.version'"),
      string(defaultValue: "", name: 'value', trim: true, description: "updated value"),
      string(defaultValue: "", name: 'commit_message', trim: true, description: "Commit message"),
      choice(name: 'environment', description: "Environment to install", choices: ["INTEGRATION", "E2E", "DEVELOPMENT", "STAGE", "PRODUCTION"])
    ])
  ])

  disableConcurrentBuilds()

  GitOpsFlow flow = new GitOpsFlow()
  try {
    flow.initWithCreditentials(<CREDTENTIAL_NAME_FOR_DEPLOYMENT_GIT_REPO>)
    flow.deploy(params)
  } catch (e) {
    currentBuild.result = 'FAILURE'
    log.error e
    throw e
  } finally {
    flow.end()
  }
}
```

Creditentials must be type `USERNAME, PASSWORD`

## Usage of `functions`

### log

```groovy
// ...
log.info 'info'
log.success 'success'
log.warning 'warning'
log.warn 'warning'
log.error 'error'
```

## Usage of classes

Syntax is like `Java` or standard `Groovy` about class usage.

```groovy
// create instance
<CLASS> variableName = new <CLASS>()
variableName.method()

// use static methods
<CLASS>.staticMethod()
```

## Contribution guidelines

- Writing tests
- Code review
- Other guidelines

## Who do I talk to?

- Repo owner or admin
- Other community or team contact

#!/usr/bin/env groovy
package com.dozenthing.jenkins

import com.dozenthing.jenkins.BitbucketApi
import com.dozenthing.jenkins.CucumberReporter
import com.dozenthing.jenkins.PushReporter
import com.dozenthing.jenkins.CucumberTestType
import com.dozenthing.jenkins.ArtifactType
import com.dozenthing.jenkins.IntegrationTest
import com.dozenthing.jenkins.Deployer

CucumberReporter cucumber
CucumberTestType testType
BuildReporter buildReporter
PushReporter pushReporter
String dockerRepo
String artifactName
ArtifactType artifactType
String artifactVersion
BitbucketApi bitbucketApi
Deployer deployer

def call(Closure body) {
    body()
}

/**
 * Checout & set env variables:
 * - GIT_BRANCH: like: origin/feature/<FEATURE>
 * - GIT_COMMIT: commit hash
 * - GIT_COMMIT_SHORT: 7 character commit hash
 * - GIT_PREVIOUS_COMMIT: commit hash
 * - GIT_PREVIOUS_SUCCESSFUL_COMMIT: commit hash
 * - GIT_URL: repo url
 * - GIT_USER: user of commit
 * - GIT_USER_EMAIL: email of commit user
 * - BITBUCKET_WORKSPACE
 * - BITBUCKET_REPO_SLUG
 *
 * Params: 
 * bitbucketCredentialsId, required
 *    - contains Jenkins credidential for Bitbucket (for reporting). 
 *    - creditentials type must be USERNAME, PASSWORD
 * nexusCredentialsId, required
 *    - contains Jenkins credidential for Nexus (for docker). 
 *    - creditentials type must be USERNAME, PASSWORD
 */
def initWithCreditentials(String BITBUCKET_CREDENTIALS_ID, String NEXUS_CREDENTIALS_ID = null) {
    log.debug "GitOpsFlow#initWithCreditentials(String, String = null)"
    this.initParamsToNull()
    env.BITBUCKET_CREDENTIALS_ID = BITBUCKET_CREDENTIALS_ID
    env.NEXUS_CREDENTIALS_ID = NEXUS_CREDENTIALS_ID

    def myRepo = checkout scm
    copyMapValuesToEnv(myRepo)
    env.GIT_COMMIT_SHORT = "${env.GIT_COMMIT[0..6]}"
    env.GIT_USER = sh(script: "git --no-pager show -s --format='%an'",returnStdout: true).trim()
    env.GIT_USER_EMAIL = sh(script: "git --no-pager show -s --format='%ae'",returnStdout: true).trim()
    env.BITBUCKET_WORKSPACE = env.GIT_URL.split("/")[3]
    env.BITBUCKET_REPO_SLUG = env.GIT_URL.split("/")[4].replace(".git", "")
}

def initParamsToNull() {
    this.deployer = null
    this.cucumber = null
    this.buildReporter = null
    this.pushReporter = null
    this.bitbucketApi = null
}


def branchStartWith(String branch) {
    log.debug "GitOpsFlow#branchStartWith(String)"
    assert branch != null
    Boolean ok = env.GIT_BRANCH.replace("origin/", "").startsWith(branch.replace("origin/", ""));
    if (!ok) {
        error("This job is not for the branch '${env.GIT_BRANCH}'")
    }
}

def branchIs(String branch) {
    log.debug "GitOpsFlow#branchIs(String)"
    assert branch != null
    Boolean ok = env.GIT_BRANCH.replace("origin/", "").equals(branch.replace("origin/", ""));
    if (!ok) {
        error("This job is not for the branch '${env.GIT_BRANCH}'")
    }
}

def start(Map params = null) {
    log.info "GitOpsFlow#start()"
    this.bitbucketApi = new BitbucketApi().withBuild(currentBuild)
    this.bitbucketApi.reportBuildStatus("INPROGRESS")
    this.params = params
}

def testBegin(CucumberTestType testType) {
    log.debug "GitOpsFlow#testBegin(CucumberTestType)"
    this.testType = testType
    this.cucumber = new CucumberReporter().init(testType, this.params)
    if (this.testType == CucumberTestType.ACCEPTANCE) {
        env.GIT_OPS_FLOW_COMMIT_TYPE = "SOURCE"
    }

}

def testEnd(boolean passed = true) {
    log.debug "GitOpsFlow#testEnd()"
    if (this.cucumber == null) {
        log.error "GitOpsFlow#testBegin(CucumberTestType) not called. Cucumber not inited."
        return
    }
    this.cucumber.createReport()
    this.cucumber.reportCucumberToBitbucket()
    this.cucumber = null
    if (passed) {
        log.success "${this.testType} test passed"
    } else {
        log.error "${this.testType} test failed"
    }
}

def end() {
    log.info "GitOpsFlow#end()"
    if (this.cucumber != null) {
        this.testEnd(false)
    }
    if (this.pushReporter != null) {
        this.endPush(false)
    }
    if (this.buildReporter != null) {
        this.buildEnd()
    }
    if (this.deployer != null) {
        this.deployer.reportToBitbucket()
        this.deployer = null
    }
    if (this.bitbucketApi == null) {
        // log.warn "GitOpsFlow#reportBuildToBitbucket(): bitbucketApi is null"
    } else {
        log.info "GitOpsFlow#reportBuildToBitbucket()"
        def status = ['SUCCESS': 'SUCCESSFUL', 'FAILURE': 'FAILED', 'UNSTABLE': 'FAILED', 'ABORTED': 'STOPPED'].get(currentBuild.currentResult)
        this.bitbucketApi.reportBuildStatus(status)
    }
}

def initWithDocker(String dockerRepo, String artifactName) {
    log.debug "GitOpsFlow#initWithDocker(String, String)"
    assert dockerRepo != null
    assert artifactName != null
    this.dockerRepo = dockerRepo
    this.artifactName = artifactName
    this.artifactType = ArtifactType.DOCKER
}

def buildBegin(ArtifactType artifactType, String artifactVersion = null) {
    log.debug "GitOpsFlow#buildBegin(ArtifactType, String)"
    assert artifactType != null
    this.artifactType = artifactType
    this.artifactVersion = artifactVersion
    this.buildReporter = new BuildReporter()
    this.buildReporter.setGitApi(this.bitbucketApi)
    this.buildReporter.setType(artifactType)
    this.buildReporter.reportInitToBitbucket()
}

def buildEnd() {
    log.debug "GitOpsFlow#buildEnd()"
    if (this.buildReporter == null) {
        log.error "GitOpsFlow#buildBegin(ArtifactType, String) not called. BuildReporter not inited."
        return
    }
    this.buildReporter.reportToBitbucket(this.getArtifactName(true))
    this.buildReporter = null
    log.success "${this.artifactType} builded: ${this.getArtifactName(true)}"
}

def getArtifactName(boolean shortName = false) {
    switch(this.artifactType) {
        case ArtifactType.DOCKER:
            if (shortName) {
                return this.artifactName + ":" + this.artifactVersion
            }
            return this.dockerRepo + "/" + this.artifactName + ":" + this.artifactVersion
        case ArtifactType.JAR:
            return this.artifactVersion
        default:
            error("GitOpsFlow#getArtifactName() not implemented for type: ${this.artifactType}")
    }
}

def pushArtifact() {
    log.debug "GitOpsFlow#pushArtifact()"
    assert this.artifactType != null

    try {
        switch(this.artifactType) {
            case ArtifactType.DOCKER:
                this.pushReporter.setUrl(pushDocker())
            break
            default:
                error("GitOpsFlow#pushArtifact() not implemented for type: ${this.artifactType}")
        }
    } catch (e) {
        log.error "${this.artifactType} push failed"
        throw e
    }
}

/**
 * GitOpsFlow#pushArtifact() is done in docker container, which do not have curl. 
 * have to call outside docker container.
 */
def beginPush() {
    this.pushReporter = new PushReporter()
    this.pushReporter.setGitApi(this.bitbucketApi)
    this.pushReporter.setParams(this.artifactType, this.getArtifactName(true), env.GIT_COMMIT_SHORT)
    this.pushReporter.setUrl(currentBuild.getAbsoluteUrl()) 
    this.pushReporter.reportToBitbucket("INPROGRESS") 
}

def endPush(boolean success = true) {
    if (success) {
        log.success "${this.artifactType} pushed"
        this.bitbucketApi.tagCommit(this.artifactVersion)
        this.pushReporter.reportToBitbucket("SUCCESSFUL") 
    } else {
        def status = ['SUCCESS': 'SUCCESSFUL', 'FAILURE': 'FAILED', 'UNSTABLE': 'FAILED', 'ABORTED': 'STOPPED'].get(currentBuild.currentResult)
        this.pushReporter.reportToBitbucket(status) 
    }
    this.pushReporter = null
}

def pushDocker() {
    log.debug "GitOpsFlow#pushDocker()"
    assert this.dockerRepo != null
    assert this.artifactName != null
    assert this.artifactVersion != null
    
    NexusApi nexus = new NexusApi()
    nexus.setCreditentials(env.NEXUS_CREDENTIALS_ID)
    String ret = nexus.pushDocker(this.dockerRepo, this.artifactName, this.artifactVersion)
    return ret
}

def containerBuilded() {
    log.debug "GitOpsFlow#containerBuilded()"
    assert this.bitbucketApi != null
    String version = this.bitbucketApi.taggedVersion()
    this.artifactVersion = version
    NexusApi nexus = new NexusApi()
    nexus.setCreditentials(env.NEXUS_CREDENTIALS_ID)
    if (!nexus.containerInNexus(this.dockerRepo, this.artifactName, this.artifactVersion)) {
        version = null
    }
    if (version != null) {
        log.success "Version found from this commit: ${this.artifactVersion}. Use that artifact. No need to build again."
    } else {
        log.info "No version found. Artifact need to build."
    }
    return version != null
}

/*
 *  FROM FLOW (GitOps implementing repo)
 */
def continueToIntegrationtest(String jobName) {
    log.debug "GitOpsFlow#continueToIntegrationtest(String)"
    IntegrationTest integrationTest = new IntegrationTest()
    integrationTest.launch(jobName, this.getArtifactName(true), this.artifactVersion)
    log.success "Integration test launched!"
}

def continueToE2eTest(String jobName) {
    log.error "GitOpsFlow#continueToE2eTest(String) NOT IMPLEMENTED!!!"
    // E2eTest e2eTest = new E2eTest()
    // e2eTest.launch(jobName, this.getArtifactName(true), this.artifactVersion)
    // log.success "E2E test launched!"
}

def deploy(Environment environment, boolean doNotDowngrade = false) {
    log.debug "GitOpsFlow#deploy(Map, Environment, boolean = false)"
    Map paramsCopy = new HashMap<String, Object>(this.params);
    paramsCopy.doNotDowngrade = doNotDowngrade
    new Deployer().initFromFlow(paramsCopy).triggerDeployment(environment)
}

/*
 *  FROM DEPLOYER (DEPLOYMENT REPO)
 */
def deploy(Map params) {
    log.debug "GitOpsFlow#deploy(Map)"
    this.deployer = new Deployer()
    this.deployer.init(params)
    this.deployer.deploy()
}


return this
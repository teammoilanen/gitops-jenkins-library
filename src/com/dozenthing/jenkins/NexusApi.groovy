#!/usr/bin/env groovy
package com.dozenthing.jenkins

String credentialsId

def call(Closure body) {
    body()
}
def setCreditentials(String credentialsId) {
    log.info "GitNexusApiOpsFlow#setCreditentials(String)"
    this.credentialsId = credentialsId
}

def containerInNexus(String dockerRepo, String dockerImage, String dockerVersion) {
    // TODO: implement!
    return true
}

def pushDocker(String dockerRepo, String dockerImage, String dockerVersion) {
    log.info "GitNexusApiOpsFlow#pushDocker(String ,String ,String)"
    assert this.credentialsId != null
    assert dockerRepo != null
    assert dockerImage != null
    assert dockerVersion != null
    withCredentials([usernamePassword(credentialsId: credentialsId, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        sh """
            echo '${PASSWORD}' | docker login -u '${USERNAME}' ${dockerRepo} --password-stdin
            docker push ${dockerRepo}/${dockerImage}:${dockerVersion}
        """
    }

    return "https://nexus.dev.12thing.com/#browse/browse:docker-private:v2%2F${dockerImage}%2Ftags%2F${dockerVersion}"
}

def containerInNexus(image, tag) {
  // TODO: implement
  return true
}

return this
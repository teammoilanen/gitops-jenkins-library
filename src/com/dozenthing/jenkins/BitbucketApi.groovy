#!/usr/bin/env groovy
package com.dozenthing.jenkins

import org.jenkinsci.plugins.workflow.support.steps.build.RunWrapper

def call(Closure body) {
    body()
}

String bitbucketCredentialsId
String bitbucketWorkspace
String bitbucketRepoSlug
String gitBranch
int buildNumber
String gitCommit

// private
def init(
    String bitbucketCredentialsId,
    String bitbucketWorkspace,
    String bitbucketRepoSlug,
    String gitBranch,
    int buildNumber,
    String gitCommit
) {
    log.debug "BitbucketApi#init(String, String, String, String, int, String)"

    this.bitbucketCredentialsId = bitbucketCredentialsId
    this.bitbucketWorkspace = bitbucketWorkspace
    this.bitbucketRepoSlug = bitbucketRepoSlug
    this.gitBranch = gitBranch
    this.buildNumber = buildNumber
    this.gitCommit = gitCommit


    assert bitbucketCredentialsId
    assert bitbucketWorkspace
    assert bitbucketRepoSlug
    assert gitBranch
    assert buildNumber
    assert gitCommit
}

def withBuild(RunWrapper build) {
    log.debug "BitbucketApi#withBuild(RunWrapper)"
    assert build != null
    BitbucketApi ret = new BitbucketApi()

    Map buildEnv = build.getBuildVariables()
    try {
        ret.init(
            buildEnv.BITBUCKET_CREDENTIALS_ID,
            buildEnv.BITBUCKET_WORKSPACE, 
            buildEnv.BITBUCKET_REPO_SLUG,
            buildEnv.GIT_BRANCH,
            build.getNumber(),
            buildEnv.GIT_COMMIT
        )
    } catch (AssertionError e) {
        log.warn "Jenkis build '${build.getFullDisplayName()}' not inited with GitOps#initWithCreditentials(...)"
        return null
    }

    return ret
}

def reportBuildStatus(String status) {
    log.info "BitbucketApi#reportStatus()"
    checkGitOpsInited()
    Map params = [:]
    params.state = status
    params.name = "${this.bitbucketRepoSlug}  » ${this.gitBranch.replace("origin/", "")} #${this.buildNumber}" 
    params.url = "${currentBuild.getAbsoluteUrl()}"
    params.key ="job-${this.bitbucketRepoSlug}-${this.buildNumber}"
    params.description = getDescription(status)

    reportBuildStatus(params)
}

def reportBuildStatus(Map params) {
    def json = convertParamsToJson(params)
    reportBuildStatusJson(json)
}

def getDescription(String status) {
    switch(status) {
        case "SUCCESSFUL":
            return "Build seems to be OK"
        case "FAILED":
            return "Build has problems!"
        case "INPROGRESS":
            return "Building..."
        case "STOPPED":
            return "Someone has stopped this build"
    }
    error("Unknown build status: ${status}. Legal values: SUCCESSFUL | FAILED | INPROGRESS | STOPPED")
}

def convertParamsToJson(Map params) {
    def json = new groovy.json.JsonBuilder()
    json params
    return groovy.json.JsonOutput.prettyPrint(json.toString())
}
    
def checkGitOpsInited() {
    if (this.bitbucketCredentialsId == null ||
        this.bitbucketWorkspace == null ||
        this.bitbucketRepoSlug == null ||
        this.gitCommit == null) {
        error("GitOps flow not inited like should be!")
    }
}

 def reportBuildStatusJson(json) {
    reportBuildStatusJson(json, this.bitbucketCredentialsId, this.bitbucketWorkspace, this.bitbucketRepoSlug, this.gitCommit)
}
/**
 * https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D/commit/%7Bnode%7D/statuses/build
 */
def reportBuildStatusJson(json, credentialsId, workspace, repoSlug, commit) {
    withCredentials([usernamePassword(credentialsId: credentialsId, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        def command = """\
            curl -s -u ${USERNAME}:${PASSWORD} \
            -H "Content-Type: application/json" \
            -X POST \
            -d '${json}' \
            https://api.bitbucket.org/2.0/repositories/${workspace}/${repoSlug}/commit/${commit}/statuses/build
        """
        sh(script: command, returnStdout: true)
    }
}

def taggedVersion() {
    log.info "BitbucketApi#taggedVersion()"
    def quote = "%22" // "/"
    String ret = null
    withCredentials([usernamePassword(credentialsId: bitbucketCredentialsId, usernameVariable: 'U', passwordVariable: 'P')]) {
        def resp = sh (
            script: "curl -s -u ${U}:${P} https://api.bitbucket.org/2.0/repositories/${this.bitbucketWorkspace}/${this.bitbucketRepoSlug}/refs/tags?q=name~${quote}-SNAPSHOT${quote}+and+target.hash=${quote}${this.gitCommit}${quote}",
            returnStdout: true
        ).trim()
        def jsonObj = readJSON text: resp
        if (jsonObj["values"] == null || jsonObj["values"].size() == 0) {
            // return null
        } else {
            ret = jsonObj["values"][0]["name"]
        }
    }
    return ret
}

def tagCommit(String tag) {
  withCredentials([usernamePassword(credentialsId: bitbucketCredentialsId, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
    def command = """\
      curl -s -u ${USERNAME}:${PASSWORD} \
      -X POST \
      -H "Content-Type: application/json" \
      -d '{
          "name" : "${tag}",
          "target" : {"hash" : "${this.gitCommit}"}
      }'\
      https://api.bitbucket.org/2.0/repositories/${this.bitbucketWorkspace}/${this.bitbucketRepoSlug}/refs/tags \
    """
    sh(script: command, returnStdout: true)
  }
}

def getCommitUrl() {
    return "https://api.bitbucket.org/2.0/repositories/${this.bitbucketWorkspace}/${this.bitbucketRepoSlug}/commits/${this.gitCommit}"
}

def getCommitUiUrl() {
    return "https://bitbucket.org/${this.bitbucketWorkspace}/${this.bitbucketRepoSlug}/commits/${this.gitCommit}"
}

return this
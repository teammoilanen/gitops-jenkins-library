#!/usr/bin/env groovy
package com.dozenthing.jenkins

enum LogLevel {
    INFO, DEBUG
}
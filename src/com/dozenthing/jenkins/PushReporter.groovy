#!/usr/bin/env groovy
package com.dozenthing.jenkins

import com.dozenthing.jenkins.ArtifactType
import com.dozenthing.jenkins.BitbucketApi

BitbucketApi bitbucketApi
ArtifactType artifactType
String artifactName
String shortCommit
String reportUrl

def call(Closure body) {
    body()
}

def setGitApi(BitbucketApi bitbucketApi) {
    log.info "PushReporter#setGitApi(BitbucketApi)"
    this.bitbucketApi = bitbucketApi
    assert this.bitbucketApi != null
}

def setUrl(String reportUrl) {
    log.info "PushReporter#setUrl(String)"
    this.reportUrl = reportUrl
    assert this.reportUrl
}

def setParams(ArtifactType artifactType, String artifactName, String shortCommit) {
    log.info "PushReporter#setParams(ArtifactType, String, String)"   
    
    this.artifactType = artifactType
    this.artifactName = artifactName
    this.shortCommit = shortCommit

    assert this.artifactType != null
    assert this.artifactName != null
    assert this.shortCommit != null
}

def getDescription(String status) {
    switch(status) {
        case "SUCCESSFUL":
            return this.artifactName
        case "FAILED":
            return "Push has problems!"
        case "INPROGRESS":
            return "Pushing..."
        case "STOPPED":
            return "Someone has stopped this build"
    }
    error("Unknown build status: ${status}. Legal values: SUCCESSFUL | FAILED | INPROGRESS | STOPPED")
}

def reportToBitbucket(String status) {
    log.info "PushReporter#reportToBitbucket(String)"
    assert status != null
    assert this.artifactName != null
    assert this.reportUrl != null

    Map params = this.createParams(status)
    this.bitbucketApi.reportBuildStatus(params)
}

def createParams(String state) {
   log.info "PushReporter#createParams(String)"
   
   Map reportParams = [:]
   reportParams.state = "${state}"
   reportParams.name = "Push artifact"
   reportParams.url = "${this.reportUrl}"
   reportParams.key ="push-${this.artifactType}-${this.shortCommit}"
   reportParams.description = "${this.getDescription(state)}"
   reportParams.refname = "Nexus/${this.artifactType}"

   return reportParams
}

return this

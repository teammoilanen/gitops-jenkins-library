#!/usr/bin/env groovy
package com.dozenthing.jenkins

enum ArtifactType {
    DOCKER, JAR
}
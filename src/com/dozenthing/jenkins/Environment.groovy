#!/usr/bin/env groovy
package com.dozenthing.jenkins

enum Environment {
    INTEGRATION, E2E, DEVELOPMENT, STAGE, PRODUCTION
}
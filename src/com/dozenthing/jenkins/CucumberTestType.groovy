#!/usr/bin/env groovy
package com.dozenthing.jenkins

enum CucumberTestType {
    ACCEPTANCE, INTEGRATION, E2E
}
#!/usr/bin/env groovy
package com.dozenthing.jenkins

import com.dozenthing.jenkins.ArtifactType
import com.dozenthing.jenkins.BitbucketApi

ArtifactType buildType
BitbucketApi bitbucketApi

def call(Closure body) {
    body()
}

def setGitApi(BitbucketApi bitbucketApi) {
   this.bitbucketApi = bitbucketApi
}

def setType(ArtifactType type) {
   log.info "BuildReporter#setType(ArtifactType ${type})"   
   assert type != null
   this.buildType = type
}

def getDescription(String status, String artifactName) {
    switch(status) {
        case "SUCCESSFUL":
            return artifactName
        case "FAILED":
            return "Build has problems!"
        case "INPROGRESS":
            return "Building..."
        case "STOPPED":
            return "Someone has stopped this build"
    }
    error("Unknown build status: ${status}. Legal values: SUCCESSFUL | FAILED | INPROGRESS | STOPPED")
}

def getUrl() {
   // if (this.reportCreated) {
   //    return currentBuild.getAbsoluteUrl() + "cucumber-html-reports/overview-features.html"
   // } else {
   //    return currentBuild.getAbsoluteUrl() + "console"
   // }
   return currentBuild.getAbsoluteUrl() 
}

def reportToBitbucket(String artifactName = null) {
   log.info "BuildReporter#reportToBitbucket()"

   def status = ['SUCCESS': 'SUCCESSFUL', 'FAILURE': 'FAILED', 'UNSTABLE': 'FAILED', 'ABORTED': 'STOPPED'].get(currentBuild.currentResult)
   Map params = createParams(status, artifactName)

   this.bitbucketApi.reportBuildStatus(params)
}

def createParams(String state, String artifactName = null) {
   log.info "BuildReporter#createParams()"
   
   Map reportParams = [:]
   reportParams.state = state
   reportParams.name = "Build artifact"
   reportParams.url = getUrl()
   reportParams.key ="build-${this.buildType}-${env.GIT_COMMIT_SHORT}"
   reportParams.description = getDescription(state, artifactName)
   reportParams.refname = "${this.buildType}"

   return reportParams
}

def reportInitToBitbucket() {
   log.info "BuildReporter#reportInitToBitbucket()"
   
   Map params = createParams("INPROGRESS")
   this.bitbucketApi.reportBuildStatus(params)

}

return this

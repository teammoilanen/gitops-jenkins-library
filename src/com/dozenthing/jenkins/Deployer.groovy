#!/usr/bin/env groovy
package com.dozenthing.jenkins

import org.jenkinsci.plugins.workflow.support.steps.build.RunWrapper

import com.dozenthing.jenkins.BitbucketApi
import com.dozenthing.jenkins.Environment

String file
String key
String value
String commitMessage
Environment environment
Boolean change
Boolean doNotDowngrade
BitbucketApi bitbucketApi
GitOpsFlow flow

def call(Closure body) {
    body()
}

/*
 * ****** GitOpsFlow -> trigger Jenkins job ******
 */
def initFromFlow(Map params) {
    log.debug "Deployer#initFromFlow(Map)"

    assert params.user
    assert params.email
    assert params.commit
    assert params.commitUrl
    assert params.project
    assert params.dockerImage

    this.user = params.user
    this.email =  params.email
    this.commit = params.commit
    this.commitUrl = params.commitUrl
    this.project = params.project
    this.dockerImage = params.dockerImage
    this.doNotDowngrade = params.doNotDowngrade

    this.bitbucketApi = new BitbucketApi().withBuild(currentBuild)

    return this
}

def triggerDeployment(Environment environment) {
    log.debug "Deployer#tiggerDeployment(Environment)"
    this.environment = environment
    this.reportInitToGit()
    this.triggerDeployment()
}

// private
def triggerDeployment() {

    assert this.bitbucketApi

    build job: 'deploy',
    parameters: [
        [$class: "StringParameterValue", name: "file", value: "${this.project}/values-${this.environment.name().toLowerCase()}.yaml"],
        [$class: "StringParameterValue", name: "key", value: "container.version"],
        [$class: "StringParameterValue", name: "value", value: this.dockerImage.split(":")[1]],
        [$class: "StringParameterValue", name: "commit_message", value: "${this.dockerImage} -> ${this.environment}"],
        [$class: "StringParameterValue", name: "environment", value: "${this.environment}"],
        [$class: "BooleanParameterValue", name: "doNotDowngrade", value: this.doNotDowngrade]
    ],
    wait: true
}

// private
def reportInitToGit() {

    def init = "Initializing..."

    Map installEnvParams = [:]
    installEnvParams.state = "INPROGRESS"
    installEnvParams.name =  "Deploy to ${this.environment}"
    installEnvParams.description = init
    installEnvParams.refname = "GitOps"
    installEnvParams.url = currentBuild.getAbsoluteUrl()
    installEnvParams.key = "deploy-${this.environment}"

    def builds = [currentBuild]
    builds.addAll(currentBuild.getUpstreamBuilds())
    builds.each { build ->
        def BitbucketApi api = this.bitbucketApi.withBuild(build)
        if (api != null) {
            api.reportBuildStatus(installEnvParams)
        }
    }
}

/*
 * ****** Deployment -> change deployment ******
 */

def init(GitOpsFlow flow) {
    this.flow = flow
    return this
}

def deploy(Environment environment) {
    this.environment = environment
    if (newerExists()) {
        log.info "Deployer#deploy(Environment) : newer version deployed. No deploy this version."
        return
    }
    Map params = [:]
    params.file = this.getDeploymentFile()
    params.key = "container.version"
    params.value = flow.getContainerVersion()
    params.commit_message = flow.getArtifactName(true) + " -> " + environment.name()
    params.environment = environment.name()

    this.init(params)
    this.deploy()
}

def getDeploymentFile() {

}

def init(Map params) {
    log.debug "Deployer#init(Map)"

    assert ! params.file.equals("")
    assert ! params.key.equals("")
    assert ! params.value.equals("")
    assert ! params.value.equals("null")
    assert ! params.commit_message.equals("")
    assert ! params.environment.equals("")

    this.file = params.file
    this.key = params.key
    this.value = params.value
    this.commitMessage = params.commit_message
    this.environment = Environment.valueOf(params.environment)
    this.doNotDowngrade = params.doNotDowngrade

    env.GIT_BRANCH = "master"
    this.change = false
    this.bitbucketApi = null
}

def deploy() {
    log.debug "Deployer#deploy()"
    assert this.value

    this.reportToBitbucket("INPROGRESS")
    
    this.change = this.update()
    if (this.change) {
        log.success "Deploy to ${this.environment} OK. New version is '${this.value}'."
        this.reportUpstramBuildsHere()
    } else {
        log.warn "Deploy to ${this.environment} not chaged. Version was already '${this.value}'."
    }
}

/*
 ************* private *************
*/



def update() {
    log.debug "Deployer#update()"

    sh 'git config --global credential.helper cache'
    sh 'git config --global push.default simple'

    sh "git config --global user.email '${getEmail()}'"
    sh "git config --global user.name '${getUser()}'"

    def datas = readYaml file: params.file
    if (!updateMap(datas, this.key, this.value)) {
        return false
    }
    if (checkDowngrading()) {
        return false
    }
    sh "rm '${this.file}'"
    writeYaml file: this.file, data: datas

    sh "git add '${this.file}'"
    sh "git commit -m '${this.commitMessage}'"
    sh "git push origin HEAD:master"
    env.GIT_COMMIT = sh(script: "git rev-parse HEAD", returnStdout:true).trim()
    this.bitbucketApi = new BitbucketApi().withBuild(currentBuild)

    return true
}

def checkDowngrading() {
    if (!doNotDowngrade) {
        return false
    }
    // 0.1.1-SNAPSHOT
    log.warn "implement 'Deployer / doNotDowngrade'"
    return false

}

// could not get dynamic access to map. this is ugly hack!
def updateMap(Map datas, String path, String value) {
    log.debug "Deployer#updateMap(Map, String, String)"
    if (path == "") {
         error("empty key provided")
    }
  def splittedPath = path.split("[.]")
  def notChaged = true
  switch(splittedPath.size()) {
      case 1: 
        notChaged = datas[splittedPath[0]] == value
        datas[splittedPath[0]] = value
      break
      case 2:
        notChaged = datas[splittedPath[0]][splittedPath[1]] == value
        datas[splittedPath[0]][splittedPath[1]] = value
      break
      case 3: 
        notChaged = datas[splittedPath[0]][splittedPath[1]][splittedPath[2]] == value
        datas[splittedPath[0]][splittedPath[1]][splittedPath[2]] = value
      break
      case 4: 
        notChaged = datas[splittedPath[0]][splittedPath[1]][splittedPath[2]][splittedPath[3]] == value
        datas[splittedPath[0]][splittedPath[1]][splittedPath[2]][splittedPath[3]] = value
      break
      case 5: 
        notChaged = datas[splittedPath[0]][splittedPath[1]][splittedPath[2]][splittedPath[3]][splittedPath[4]] == value
        datas[splittedPath[0]][splittedPath[1]][splittedPath[2]][splittedPath[3]][splittedPath[4]] = value
      break
      case 6: 
        notChaged = datas[splittedPath[0]][splittedPath[1]][splittedPath[2]][splittedPath[3]][splittedPath[4]][splittedPath[5]] == value
        datas[splittedPath[0]][splittedPath[1]][splittedPath[2]][splittedPath[3]][splittedPath[4]][splittedPath[5]] = value
      break
      case 7: 
        notChaged = datas[splittedPath[0]][splittedPath[1]][splittedPath[2]][splittedPath[3]][splittedPath[4]][splittedPath[5]][splittedPath[6]] == value
        datas[splittedPath[0]][splittedPath[1]][splittedPath[2]][splittedPath[3]][splittedPath[4]][splittedPath[5]][splittedPath[6]] = value
      break
      default:
        error("too deep map access")
  }
  return !notChaged
}

def getEmail() {
    def rootBuild = getRootBuild()
    if (rootBuild == null) {
        return 'jenkins.12thing@gmail.com'
    }
    return rootBuild.getBuildVariables().GIT_USER_EMAIL
}

def getUser() {
    def rootBuild = getRootBuild()
    if (rootBuild == null) {
        return 'Jenkins'
    }
    return rootBuild.getBuildVariables().GIT_USER
}

def getRootBuild() {
    log.debug "Deployer#getRootBuild()"
    List<RunWrapper> builds = currentBuild.getUpstreamBuilds()
    if (builds.isEmpty()) {
        return null
    }
    return builds.get(builds.size()-1)
}

def getDescription(String status) {
    switch(status) {
        case "INIT":
            return "Initializing..."
        case "INPROGRESS":
            return "Deploying..."
        case "SUCCESSFUL":
            return "Deploy OK" // versio | no change
        case "FAILED":
            return "Deploy has problems!"
        case "STOPPED":
            return "Someone has stopped this build"
    }
    error("Unknown build status: ${status}. Legal values: SUCCESSFUL | FAILED | INPROGRESS | STOPPED")
}


def reportUpstramBuildsHere() {
    log.debug "Deployer#reportUpstramBuildsHere()"
    def builds = []
    builds.addAll(currentBuild.getUpstreamBuilds())
    builds.reverse().each { build ->
        reportUpstramBuildHere(build)
    }
}

def reportUpstramBuildHere(RunWrapper build) {

    def label = build.getBuildVariables()["DEPLOY_LABEL"]
    if (label == null) {
        label = ""
    }
    // def url = "${build.getAbsoluteUrl()}cucumber-html-reports/overview-features.html"

    Map params = [:]
    params.state = "SUCCESSFUL"
    params.name = build.getFullDisplayName()
    // description": ${this.change}", //...
    params.url = build.getAbsoluteUrl()
    params.refname = "CUCUMBER"
    params.key = build.getId()

    this.bitbucketApi.reportBuildStatus(params)
}

def reportToUpstream(String status) {
    assert status
    def builds = []
    builds.addAll(currentBuild.getUpstreamBuilds())
     builds.each { build ->
        def BitbucketApi api = new BitbucketApi().withBuild(build)
        if (api != null) {
            api.reportBuildStatus(params)
        }
    }
}

/**
 * Reprts deploy job status for itself and upstream jobs
 *
 */
def reportToBitbucket(String status = null) {
    log.debug "Deployer#reportToBitbucket(String = null)"

    def jobStatus = status != null ? status : ['SUCCESS': 'SUCCESSFUL', 'FAILURE': 'FAILED', 'UNSTABLE': 'FAILED', 'ABORTED': 'STOPPED'].get(currentBuild.currentResult)

    def builds = []
    if (this.change) {
        builds.add(currentBuild)
    }
    builds.addAll(currentBuild.getUpstreamBuilds())
    builds.each { build ->
       def BitbucketApi api = new BitbucketApi().withBuild(build)
        if (api != null) {
            Map params = createParams(jobStatus, api)
            api.reportBuildStatus(params)
        }
    }
}

def createParams(String state, BitbucketApi api) {
   Map reportParams = [:]
   reportParams.state = state == "INIT" ? "INPROGRESS" : state
   reportParams.name = "Deploy to ${this.environment}"
   reportParams.description = getDescription(state)
   reportParams.refname = "GitOps"
   reportParams.url = getUrl(api)
   reportParams.key ="deploy-${this.environment}"
   return reportParams
}

def getUrl(BitbucketApi api) {
    if (!this.change || api.gitCommit.equals(env.GIT_COMMIT)) {
        return currentBuild.getAbsoluteUrl() 
    }
    return this.bitbucketApi.getCommitUiUrl()
}

return this

#!/usr/bin/env groovy
package com.dozenthing.jenkins

import java.io.Serializable;

public class AnsiColors implements Serializable {
    static String RESET = "\u001b[0m"
    static Map DECORATIONS = [
        Bold: "\u001b[1m", 
        Underline: "\u001b[4m",
        Reversed: "\u001b[7m"
    ]
    static Map COLORS = [
        Black: "\u001b[30m",
        Red: "\u001b[31m",
        Green: "\u001b[32m",
        Yellow: "\u001b[33m",
        Blue: "\u001b[34m",
        Magenta: "\u001b[35m",
        Cyan: "\u001b[36m",
        White: "\u001b[37m"
    ]
    static Map BACKGROUND_COLORS = [
        Black: "\u001b[40m",
        Red: "\u001b[41m",
        Green: "\u001b[42m",
        Yellow: "\u001b[43m",
        Blue: "\u001b[44m",
        Magenta: "\u001b[45m",
        Cyan: "\u001b[46m",
        White: "\u001b[47m"
    ]
}
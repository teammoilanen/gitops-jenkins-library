#!/usr/bin/env groovy
package com.dozenthing.jenkins

import org.jenkinsci.plugins.workflow.support.steps.build.RunWrapper

import com.dozenthing.jenkins.CucumberTestType

CucumberTestType testType
BitbucketApi bitbucketApi
Boolean reportCreated
String fileIncludePattern
Map params

def call(Closure body) {
    body()
}

def init(CucumberTestType testType, Map params) {
   log.debug "CucumberReporter#init(CucumberTestType, Map)"   
   assert testType != null
   this.testType = testType
   this.reportCreated = false
   this.fileIncludePattern = "target/cucumber.json"
   
   this.params = params
   this.bitbucketApi = new BitbucketApi().withBuild(currentBuild)
   this.reportInitToBitbucket()

   if (testType == CucumberTestType.ACCEPTANCE) {
      return this
   }

   assert ! params.isEmpty()


   if (testType == CucumberTestType.INTEGRATION) {
      assert ! params.isEmpty()
      assert params.user
      assert params.email
      assert params.commit
      assert params.commitUrl
      assert params.project
      assert params.dockerImage
      return this
   } 
   return this

}

def createReport() {
   log.debug "CucumberReporter#createReport()"   
   // tähän pikkusen eri asetukset tyypin mukaan...
   def commitUrl = "https://bitbucket.org/${env.BITBUCKET_WORKSPACE}/${env.BITBUCKET_REPO_SLUG}/commits/${env.GIT_COMMIT}"
   switch(this.testType) {
      case CucumberTestType.ACCEPTANCE:
         createCucumberAcceptanceReport(env.BITBUCKET_REPO_SLUG, commitUrl, env.GIT_COMMIT_SHORT, env.GIT_USER)
         break
      case CucumberTestType.INTEGRATION:
         createCucumberIntegrationReport(params.project, params.dockerImage, params.commitUrl, params.commit, commitUrl, env.GIT_COMMIT_SHORT, params.user, env.GIT_USER)
         break
      default: log.error "not implemented: CucumberTestType.${this.testType}"
   }
}  

def createCucumberIntegrationReport(projectName, dockerImage, codeCommitUrl, codeCommitHash, testCommitUrl, testCommitHash, codeUser, testUser) {
   log.debug "CucumberReporter#createCucumberIntegrationReport(String, String, String, String)"
   cucumber buildStatus: "${currentBuild.currentResult}",
      failedFeaturesNumber: 1,
      failedScenariosNumber: 1,
      skippedStepsNumber: 1,
      failedStepsNumber: 1,
      classifications: [
         [key: 'Tested', value: projectName],
         [key: 'Code commit', value: "<a target=_blank href='${codeCommitUrl}'>${codeCommitHash[0..6]}</a>"],
         [key: 'Code submitter', value: codeUser],
         [key: 'Container to test', value: dockerImage],
         [key: 'Test commit', value: "<a target=_blank href='${testCommitUrl}'>${testCommitHash}</a>"],
         [key: 'Test submitter', value: testUser]
      ],
      fileIncludePattern: fileIncludePattern,
      sortingMethod: 'NATURAL',
      trendsLimit: 100
   this.reportCreated = true
} 

def createCucumberAcceptanceReport(projectName, commitUrl, shortGitCommit, user) {
   log.debug "CucumberReporter#createCucumberAcceptanceReport(String, String, String, String)"
   cucumber buildStatus: "${currentBuild.currentResult}",
      failedFeaturesNumber: 1,
      failedScenariosNumber: 1,
      skippedStepsNumber: 1,
      failedStepsNumber: 1,
      classifications: [
         [key: 'Repository', value: projectName],
         [key: 'Commit', value: "<a target=_blank href='${commitUrl}'>${shortGitCommit}</a>"],
         [key: 'Submitter', value: user]
      ],
      fileIncludePattern: fileIncludePattern,
      sortingMethod: 'NATURAL',
      trendsLimit: 100
   this.reportCreated = true
} 

def getDescription(String status) {
    switch(status) {
        case "SUCCESSFUL":
            return "Tests seems to be OK"
        case "FAILED":
            return "Tests has problems!"
        case "INPROGRESS":
            return "Testing..."
        case "STOPPED":
            return "Someone has stopped this build"
    }
    error("Unknown build status: ${status}. Legal values: SUCCESSFUL | FAILED | INPROGRESS | STOPPED")
}

def getUrl() {
   if (this.reportCreated) {
      return currentBuild.getAbsoluteUrl() + "cucumber-html-reports/overview-features.html"
   } else {
      return currentBuild.getAbsoluteUrl() + "console"
   }
}

def getBitbucketName() {
   switch(testType) {
      case CucumberTestType.ACCEPTANCE:
         return "Acceptance test"
      case CucumberTestType.INTEGRATION:
         return "Integration test"
      case CucumberTestType.E2E:
         return "E2E test"
   }
}

def reportCucumberToBitbucket() {
   log.debug "CucumberReporter#reportCucumberToBitbucket()"

   def status = ['SUCCESS': 'SUCCESSFUL', 'FAILURE': 'FAILED', 'UNSTABLE': 'FAILED', 'ABORTED': 'STOPPED'].get(currentBuild.currentResult)
   List<RunWrapper> builds = [currentBuild]
   builds.addAll(currentBuild.getUpstreamBuilds())
   builds.each { build ->
      BitbucketApi api = new BitbucketApi().withBuild(build)
      if (api != null) {
         Map params = createParams(status, build, api)
         api.reportBuildStatus(params)
      }
   }
}

def createParams(String state, RunWrapper build, BitbucketApi api) {
   log.debug "CucumberReporter#createParams()"
   
   Map reportParams = [:]
   reportParams.state = state
   reportParams.name = getBitbucketName() 
   reportParams.description = getDescription(state)
   reportParams.refname = "CUCUMBER"
   reportParams.url = getUrl()
   if (api.gitCommit == env.GIT_COMMIT && this.testType != CucumberTestType.ACCEPTANCE) {
      // integration && e2e tests
      reportParams.key ="${testType}-${env.GIT_COMMIT_SHORT}"
   } else {
      // upstream builds && acceptance tests
      reportParams.key ="${testType.name().toLowerCase()}-test"
   }
   return reportParams
}

def reportInitToBitbucket() {
   log.debug "CucumberReporter#reportInitToBitbucket()"
   List<RunWrapper> builds = [currentBuild]
   builds.addAll(currentBuild.getUpstreamBuilds())
   builds.each { build ->
      BitbucketApi api = new BitbucketApi().withBuild(build)
      if (api != null) {
         Map params = createParams("INPROGRESS", build, api)
         api.reportBuildStatus(params)
      }
   }

}

return this

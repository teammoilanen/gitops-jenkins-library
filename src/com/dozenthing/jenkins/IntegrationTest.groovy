#!/usr/bin/env groovy
package com.dozenthing.jenkins

import org.jenkinsci.plugins.workflow.support.steps.build.RunWrapper

import com.dozenthing.jenkins.BitbucketApi
import com.dozenthing.jenkins.Environment

BitbucketApi bitbucketApi
String jobName
String artifactName
String artifactVersion

def call(Closure body) {
    body()
}

def launch(String jobName, String artifactName, String artifactVersion) {
    log.debug "IntegrationTest#launch(String, String, String)"
    log.debug jobName
    log.debug artifactName
    log.debug artifactVersion

    this.jobName = jobName
    this.artifactName = artifactName
    this.artifactVersion = artifactVersion
    
    this.bitbucketApi = new BitbucketApi().withBuild(currentBuild)
    this.reportInitToBitbucket()

    this.launchIntegrationJob()
}

// private
def launchIntegrationJob() {

    BitbucketApi api = new BitbucketApi().withBuild(currentBuild)
    assert api
    
    build job: this.jobName, 
        parameters: [
            [$class: "StringParameterValue", name: "user", value: "${env.GIT_USER}"],
            [$class: "StringParameterValue", name: "email", value: "${env.GIT_USER_EMAIL}"],
            [$class: "StringParameterValue", name: "commit", value: "${env.GIT_COMMIT}"],
            [$class: "StringParameterValue", name: "commitUrl", value: "${api.getCommitUiUrl()}"],
            [$class: "StringParameterValue", name: "project", value: "${env.BITBUCKET_REPO_SLUG}"],
            [$class: "StringParameterValue", name: "dockerImage", value: "${this.artifactName}"],
        ],
        wait: false
}

def getDescription(String status) {
    switch(status) {
        case "INIT":
            return "Initializing..."
        case "INPROGRESS":
            return "Testing..."
        case "SUCCESSFUL":
            return "Tests seems to be OK"
        case "FAILED":
            return "Tests has problems!"
        case "STOPPED":
            return "Someone has stopped this build"
    }
    error("Unknown build status: ${status}. Legal values: SUCCESSFUL | FAILED | INPROGRESS | STOPPED")
}

def getUrl() {
    if (env.GIT_COMMIT)
   // if (this.reportCreated) {
   //    return currentBuild.getAbsoluteUrl() + "cucumber-html-reports/overview-features.html"
   // } else {
   //    return currentBuild.getAbsoluteUrl() + "console"
   // }
   return currentBuild.getAbsoluteUrl() 
}

def reportInitToBitbucket() {

    def init = "Initializing..."

    Map installEnvParams = [:]
    installEnvParams.state = "INPROGRESS"
    installEnvParams.name =  "Deploy to ${Environment.INTEGRATION}"
    installEnvParams.description = init
    installEnvParams.refname = "GitOps"
    installEnvParams.url = currentBuild.getAbsoluteUrl()
    installEnvParams.key = "deploy-${Environment.INTEGRATION}"

    Map doTestParams = [:]
    doTestParams.state = "INPROGRESS"
    doTestParams.name = "Integration test" 
    doTestParams.description = init
    doTestParams.refname = "CUCUMBER"
    doTestParams.url = currentBuild.getAbsoluteUrl()
    doTestParams.key = "integration-test"

    def builds = [currentBuild]
    builds.addAll(currentBuild.getUpstreamBuilds())
    builds.each { build ->
        def BitbucketApi api = this.bitbucketApi.withBuild(build)
        if (api != null) {
            api.reportBuildStatus(installEnvParams)
            api.reportBuildStatus(doTestParams)
        }
    }
}

def reportToBitbucket(String status = null) {
    log.info "IntegrationTest#reportToBitbucket()"

    def jobStatus = status != null ? status : ['SUCCESS': 'SUCCESSFUL', 'FAILURE': 'FAILED', 'UNSTABLE': 'FAILED', 'ABORTED': 'STOPPED'].get(currentBuild.currentResult)

    Map params = createParams(jobStatus)
    def builds = [currentBuild]
    builds.addAll(currentBuild.getUpstreamBuilds())
    builds.each { build ->
        def BitbucketApi api = this.bitbucketApi.withBuild(build)
        if (api != null) {
            api.reportBuildStatus(params)
        }
    }
}

def createParams(String state) {
   log.info "IntegrationTest#createParams()"

   Map reportParams = [:]
   reportParams.state = state
   reportParams.name = "Integration test" // tähän ehkä dev-commitin tietoa...
   reportParams.description = getDescription(state)
   reportParams.refname = "CUCUMBER"
   reportParams.url = getUrl()
   reportParams.key = "integration-test"

   return reportParams
}

return this

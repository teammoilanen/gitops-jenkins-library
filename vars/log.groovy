#!/usr/bin/env groovy

import com.dozenthing.jenkins.AnsiColors

def call(Closure body) {
  body()
}

def call(message) {
  info(message)
}

def debug(message) {
  printlog(AnsiColors.COLORS.Magenta, "DEBUG", message)
}
def success(message) {
  printlog(AnsiColors.COLORS.Green, "SUCCESS", message)
}

def info(message) {
  printlog(AnsiColors.COLORS.Blue, "INFO", message)
}

def warning(message) {
  printlog(AnsiColors.COLORS.Yellow, "WARNING", message)
}

def warn(message) {
  warning(message)
}

def error(message) {
  printlog(AnsiColors.COLORS.Red, "ERROR", message)
}

def printlog(ansi, type, message) {
  ansiColor('xterm') {
    echo "${ansi}${AnsiColors.DECORATIONS.Reversed}[ ${type} ]${AnsiColors.RESET}: ${message}"
  }
}

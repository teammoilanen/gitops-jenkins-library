#!/usr/bin/env groovy

def call(Closure body) {
  body()
}

def call(String name, Map map) {
  assert name != null
  log.info "iterating over Map ${name}:"
  if (map == null) {
    log.warn "iterateOverMap: map is null"
    return
  }
  map.each { key, value ->
    echo "  ${key}: ${value}"
  }
}
#!/usr/bin/env groovy

def call(Closure body) {
  body()
}

def call(Map map) {
  if (map == null) {
    log.warn "copyMapValuesToEnv(Map): map is null"
    return
  }

  map.each { key, value ->
    env[key] = value
  }
}